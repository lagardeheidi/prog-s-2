#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow *w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChildIndex(int nodeIndex) // Retourne l'indice du fils a gauche du noeud a l'indice donne dans le tableau du tas
{
	return nodeIndex * 2 + 1;
}

int Heap::rightChildIndex(int nodeIndex) // idem a droite
{
	return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value) // insere un noeud avec une valeur donne dans le tas binaire
{
	int i = heapSize;
	(*this)[i] = value;
	while (i > 0 && (*this)[i] > (*this)[(i - 1) / 2])
	{
		swap(i, (i - 1) / 2);
		i = (i - 1) / 2;
	}
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	int i_max = nodeIndex; // initialise l_max avec l indice du noeud actuel
	int RightValue = -1;
	int LeftValue = -1;
	int indexRight = rightChildIndex(nodeIndex);
	int indexLeft = leftChildIndex(nodeIndex);

	// verifie la taille avant d'attribuer la valeur
	if (indexRight < heapSize) // si l indice du fils droit est < a la taille du tas
	{
		RightValue = (*this)[rightChildIndex(nodeIndex)]; // la valeur droite est mise a jour avec la valeur du fils droit dans le tableau
	}
	else
	{
		RightValue = -1; // sinon elle a pas de valeur valide
	}

	if (indexLeft < heapSize) // idem avec le fils gauche
	{
		LeftValue = (*this)[leftChildIndex(nodeIndex)];
	}
	else
	{
		LeftValue = -1;
	}

	if ((*this)[nodeIndex] < RightValue) // si la valeur du noeud actuel est < a la valeur du fils droit
	{
		i_max = indexRight; // alors la valeur du fils droit est plus grande donc on met a jour la variable
	}

	if ((*this)[i_max] < LeftValue) // idem a gauche
	{
		i_max = indexLeft;
	}

	if (i_max != nodeIndex)		  // si imax est different de l indice du noeud actuel
	{							  // alors ya une valeur plus grande parmi les fils
		swap(nodeIndex, i_max);	  // on echange la valeur du noeud actuel avec la valeur maximal parmi les fils
		heapify(heapSize, i_max); // on appelle recurcivement la fonction heapify pour continuer a reorganiser le sous arbre
	}
}

void Heap::buildHeap(Array &numbers)
{
	int n = numbers.size();
	for (int i = 0; i < n; i++)
	{
		insertHeapNode(n, numbers[i]);
	}
}

void Heap::heapSort()
{
	for (int i = (*this).size() - 1; i >= 0; i--)
	{
		swap(0, i);
		heapify(i, 0);
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
	w = new HeapWindow();
	w->show();

	return a.exec();
}
