#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow *w = nullptr;

void bubbleSort(Array &toSort)
{
	// bubbleSort
	// Pour chaque indice i de t faire
	for (int i = 0; i < toSort.size() - 1; i++)
	{
		// Pour chaque cases adjacentes faire
		for (int j = 0; j < toSort.size() - i - 1; j++)
		{
			// remonter la valeur la plus haute
			if (toSort[j] > toSort[j + 1])
			{
				toSort.swap(j, j + 1);
			}
		} // fin Pour
	}	  // fin Pour
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount = 20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
