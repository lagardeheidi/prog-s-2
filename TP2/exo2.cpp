#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow *w = nullptr;

void insertionSort(Array &toSort)
{
	// toSort ⇐ tableau de nombre aléatoire
	Array &sorted = w->newArray(toSort.size());
	// result[0] ⇐ toSort[0]
	sorted[0] = toSort[0];
	// Pour chaque nombre i de toSorted (excepté le premier) faire
	for (int i = 1; i < toSort.size(); i++)
	{
		int index = -1; // les cases de sorted sont vides (toutes=-1 au départ)
		int j = 0;
		// Si ∃m ∈ result tel que j > i Alors
		while (j < i && sorted[j] < toSort[i])
		{
			j++;
		}
		index = j;
		// insérer i à la position de j (en décalant donc le reste du tableau)

		if (index >= 0)
		{
			for (int position = i; position > index; position--)
			{
				sorted[position] = sorted[position - 1];
			}
			sorted[index] = toSort[i];
		}
		else // Sinon
		{
			sorted[i] = toSort[i]; // insérer i à la fin
		}						   // fin Si
	}							   // fin Pour
	toSort = sorted;			   // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount = 15;					// number of elements to sort
	MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
	w = new TestMainWindow(insertionSort);	// window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
