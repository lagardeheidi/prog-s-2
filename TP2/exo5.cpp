#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow *w = nullptr;

void merge(Array &first, Array &second, Array &result);
void splitAndMerge(Array &origin)
{
	if (origin.size() <= 1) // verifie si le tableau est déjà trié
	{
		return;
	}
	// creation de deux nouveaux tableaux

	// sa taille est la taille du tableau origin/2
	Array &first = w->newArray(origin.size() / 2);
	// sa taille est la taille de origin moins la taille de first
	Array &second = w->newArray(origin.size() - first.size());

	// split copie les elements de origin dans les tableaux first et second
	for (int i = 0; i < first.size(); i++)
	{
		first[i] = origin[i];
	}
	for (int j = 0; j < second.size(); j++)
	{
		second[j] = origin[first.size() + j];
	}
	// appel recurcif de la fonction splitAndMerge sur les sous-tableaux first et second pour les trier
	splitAndMerge(first);
	splitAndMerge(second);

	// merge fusionner first et second en un seul tableau origin
	merge(first, second, origin);
}

void merge(Array &first, Array &second, Array &result) // prend en parametre les tableaux results, first et second
{
	// initialise deux indices a 0 pour suivre la position actuelle dans les tableaux first et second
	int index1 = 0;
	int index2 = 0;
	// boucle qui permet de fusionner first et scond et met dans result
	// boucle continue tant que index1 et index2 a pas atteint la fin de leur propre tableau
	while (index2 != first.size() || index1 != second.size())
	{
		if (index2 == first.size())
		{
			result[index2 + index1] = second[id];
			index1++;
		}
		else if (index1 == second.size())
		{
			result[index2 + index1] = first[index2];
			index1++;
		}
		else if (first[index2] < second[index1])
		{
			result[index2 + index1] = first[index2];
			index2++;
		}
		else
		{
			result[index2 + index1] = second[index1];
			index1++;
		}
	}
}

void mergeSort(Array &toSort)
{
	splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
	w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
