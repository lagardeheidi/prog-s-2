#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow *w = nullptr;

void recursivQuickSort(Array &toSort, int size)
{

	// stop statement = condition + return (return stop the function even if it does not return anything)

	if (size <= 1) // si le tableau est déjà trie, on arrete
	{
		return;
	}
	// on coupe en deux le tableau
	Array &lowerArray = w->newArray(size);	 // tableau avec elements inferieur au pivot
	Array &greaterArray = w->newArray(size); // tableau avec elements superieur au pivot
	int lowerSize = 0, greaterSize = 0;		 // effectives sizes

	// split trier lowers et greaters
	int pivot = toSort[0];		   // on choisi un element du tableau comme pivot (ici la premiere case)
	for (int i = 1; i < size; i++) // on parcourt le tableau
	{
		if (toSort[i] < pivot)
		{
			lowerArray[lowerSize] = toSort[i]; // si un element est inferieur au pivot, on le met dans le tab inferieur
			lowerSize++;					   // on augmente la taille du tab au fur et a mesure
		}
		else
		{
			greaterArray[greaterSize] = toSort[i]; // si un element est superieur au pivot, on le met dans le tab superieur
			greaterSize++;						   // on augmente la taille du tab au fur et a mesure
		}
	}
	// recursiv sort of lowerArray and greaterArray
	recursivQuickSort(lowerArray, lowerSize); // on appelle recursivement la fonction sur nos deux tableaux
	recursivQuickSort(greaterArray, greaterSize);
	// merge fusionner lowers, pivot et greaters
	for (int j = 0; j < lowerSize; j++)
	{
		toSort[j] = lowerArray[j]; // on copie les element lower dans toSort pour commencer
	}

	toSort[lowerSize] = pivot; // on place le pivot dans toSort[lowerSize]

	for (int k = 0; k < greaterSize; k++)
	{
		toSort[lowerSize + k + 1] = greaterArray[k]; // pour finir on copie les elements greater dans toSort
	}
}

void quickSort(Array &toSort)
{
	recursivQuickSort(toSort, toSort.size());
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount = 20;
	MainWindow::instruction_duration = 50;
	w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
