#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow *w = nullptr;

void selectionSort(Array &toSort)
{
    // selectionSort

    for (int i = 0; i < toSort.size(); i++)
    {
        int min = toSort[i]; // on affecte la valeur de l'element actuel au minimum
        int indexMin = i;    // on affecte i à l'index du minumin (la place du minimum)
        for (int j = i + 1; j < toSort.size(); j++)
        {
            if (min > toSort[j])
            {                    // si le minimum (la valeur actuelle du i) est > à la valeur du j
                min = toSort[j]; // on dit que le minimum est egale à la valeur du j
                indexMin = j;    // on indique la place du minimum (ici j)
            }
        }
        toSort.swap(indexMin, i); // après ca, on échange les places du indexMin et du i
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    uint elementCount = 15;                 // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort);  // window which display the behavior of the sort algorithm
    w->show();

    return a.exec();
}
