#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow *w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	 * Make a graph from a matrix
	 * first create all nodes, add it to the graph then connect them
	 * this->appendNewNode
	 * this->nodes[i]->appendNewEdge
	 */
	for (int i = 0; i < nodeCount; i++) // pour chaque ligne
	{
		GraphNode *node = new GraphNode(i); // cree un nouveau noeud avec l indice de la ligne comme valeur
		this->appendNewNode(node);			// ajoute ce noeud au graphe
	}

	for (int i = 0; i < nodeCount; i++) // pour chaque ligne
	{
		for (int j = 0; j < nodeCount; j++)
		{
			if (adjacencies[i][j] != 0) // pour chaque element non nul
			{
				this->nodes[i]->appendNewEdge(this->nodes[j], adjacencies[i][j]); // ajoute une arete entre le noeud correspondant a la ligne et le noeud correspondant a la colonne
			}
		}
	}
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using recursivity
	 */
	nodes[nodesSize] = first;	  // ajoute le noeud first au tableau node
	visited[first->value] = true; // le definie comme  a ete visite
	nodesSize++;

	for (Edge *e = first->edges; e != NULL; e = e->next) // pour chaque arrete
	{
		if (!visited[e->destination->value]) // si le noeud de destination de l arete a pas ete visite,
		{
			deepTravel(e->destination, nodes, nodesSize, visited); // appelle recurcivement la fonction deepTravel. On passe le noeud de destination comme le nouveau noeud de depart
		}
	}
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode *> nodeQueue;
	nodeQueue.push(first);
	while (nodeQueue.size() != 0) // tant que la file d attente n est pas vide
	{
		GraphNode *noeud = nodeQueue.front(); // retire le premier noeud de la file
		nodeQueue.pop();
		nodes[nodesSize] = noeud;	  // ajoute ce noeud au tableau nodes
		nodesSize++;				  // et met a jour la taille du tableau
		visited[noeud->value] = true; // marque le noeud comme a ete visite

		for (Edge *edge = noeud->edges; edge != NULL; edge = edge->next) // pour chaque arete
		{
			if (visited[edge->destination->value] == false) // si le noeud a pas ete visite
			{
				nodeQueue.push(edge->destination); // on l ajoute a la file d attente
			}
		}
	}
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
	std::queue<GraphNode *> nodeQueue;
	nodeQueue.push(first);

	while (nodeQueue.size() != 0) // tant que la file attente n est pas vide
	{
		GraphNode *noeud = nodeQueue.front(); // retire le premier noeud de la file attente
		nodeQueue.pop();
		visited[noeud->value] = true; // marque le noeud comme a ete visite

		for (Edge *edge = noeud->edges; edge != NULL; edge = edge->next) // pour les aretes du noeud
		{
			if (visited[edge->destination->value] == false) // si le noeud a pas ete visite
			{
				nodeQueue.push(edge->destination); // ajoute a la file d attente
			}
			else
			{
				return true; // si a ete visite
			}
		}
	}

	return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
