#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow *w = nullptr;
using std::size_t;

int binarySearch(Array &array, int toSearch)
{
	// Initialise indice de début de recherche binaire a 0
	int start = 0;
	// Initialise indice de fin de recherche binaire a la taille du tableau
	int end = array.size();
	// Initialise indice de la valeur recherche a -1 -> valeur pas encore trouve
	int index = -1;

	while (start < end) // tant que indice debut est inferieur a indice de fin
	{
		int middle = (start + end) / 2; // calcul le millieu de l invervalle entre start et end
		if (toSearch > array[middle])	// verifie si la valeur recherchee toSearch est > a la valeur du tableau a indice middle
		{								// Si oui, alors la valeur recherchee se trouve dans la premiere moitie de l intervalle
			start = middle + 1;			// met a jour indice start
		}
		else if (toSearch < array[middle]) // verifie si la valeur recherchee toSearch est < a la valeur du tableau a indice middle
		{
			end = middle; // met a jour end
		}
		else // si aucune des conditions sont vraies la valeur a ete trouve
		{
			index = middle;
			return index;
		}
	}
	return -1;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchWindow(binarySearch);
	w->show();

	return a.exec();
}
