#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow *w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{
    SearchTreeNode *left;
    SearchTreeNode *right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left = NULL;
        this->right = NULL;
        this->value = value;
    }

    void insertNumber(int value) // insert un nouveau noeud dans l arbre de recherche
    {
        // create a new node and insert it in right or left child
        if (value > this->value) // Si la valeur a inserer est > a la valeur du noeud actuel alors elle est inseree dans le noeud de droite
        {
            if (this->right == NULL)
            {
                this->right = (SearchTreeNode *)malloc(sizeof(Node));
                this->right = (SearchTreeNode *)createNode(value);
            }
            else
            {
                this->right->insertNumber(value);
            }
        }
        else // sinon va a gauche
        {
            if (this->left == NULL)
            {
                this->left = (SearchTreeNode *)malloc(sizeof(Node));
                this->left = (SearchTreeNode *)createNode(value);
            }
            else
            {
                this->left->insertNumber(value);
            }
        }
    }

    uint height() const // calcule la hauteur de l arbre
    {
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int RightHeight = 0;
        int LeftHeight = 0;

        if (this->left == NULL && this->right == NULL) // Si le noeud a pas d enfants alors la hauteur retournee est 1
        {
            return 1;
        }
        else
        {
            if (this->left != NULL) // si le sous arbre gauche existe
            {
                LeftHeight += this->left->height(); // appelle recurcivement la fonction height sur le sous arbre gauche et ajoute sa hauteur à la variable
            }
            if (this->right != NULL) // idem pour le sous arbre de droite
            {
                RightHeight += this->right->height();
            }
            if (RightHeight > LeftHeight) // on compare les hauteurs des deux sous arbres
            {
                return RightHeight + 1;
            }
            else
            {
                return LeftHeight + 1;
            }
        }
    }

    uint nodesCount() const // calcule le nb total de noeuds dans l arbre
    {
        int nbRightNode = 0;
        int nbLeftNode = 0;

        if (this->left == NULL && this->right == NULL)
        {
            return 1;
        }
        else
        {
            if (this->left != NULL) // si le sous arbre gauche existe
            {
                nbLeftNode += this->left->nodesCount(); // appelle recurcivement la fonction nodesCount sur le sous arbre gauche et ajoute le nb de noeuds retourne à la variable
            }
            if (this->right != NULL) // idem a droite
            {
                nbRightNode += this->right->nodesCount();
            }
            return nbRightNode + nbLeftNode + 1;
        }
    }

    bool isLeaf() const
    {
        // return True if the node is a leaf (it has no children)
        if (this->right == NULL && this->left == NULL) // regarde si le noeud a un enfant ou non
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void allLeaves(Node *leaves[], uint &leavesCount)
    {
        // fill leaves array with all leaves of this tree
        if (this->isLeaf() == true) // verifie si le noeud est une feuille ou non
        {
            leaves[leavesCount] = this; // si oui alors il est ajoute au tableau leaves a lendroit ou est leavesCount
            leavesCount++;              // indique qu une nouvelle feuille est ajoute
        }
        else // si le noeud est pas une feuille
        {
            if (this->left != NULL) // si le sous arbre gauche existe
            {
                this->left->allLeaves(leaves, leavesCount); // appelle recurcivement la focntion allLeaves sur le sous arbre gauche
            }
            if (this->right != NULL) // idem a droite
            {
                this->right->allLeaves(leaves, leavesCount);
            }
        }
    }

    void inorderTravel(Node *nodes[], uint &nodesCount) // rempli le tab Nodes avec tous les noeuds de larbre en faisant un parcours dans lordre
    {
        // fill nodes array with all nodes with inorder travel
        if (this->left != NULL) // verifie si le sous arbre gauche existe
        {
            this->left->inorderTravel(nodes, nodesCount); // appelle recurcivement blablabla
        }

        nodes[nodesCount] = this; // le noeud actuel est ajoute au tab nodes a l endfroit ou se situe l indice nodesCount
        nodesCount++;             // increment nodesCount

        if (this->right != NULL) // idem a droite
        {
            this->right->inorderTravel(nodes, nodesCount);
        }
    }

    void preorderTravel(Node *nodes[], uint &nodesCount) // fait cette fois un parcours preordonne
    {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;

        if (this->left != NULL)
        {
            this->left->preorderTravel(nodes, nodesCount); // appelle recurcivement blabla (cf les autres fonctions)
        }

        if (this->right != NULL)
        {
            this->right->preorderTravel(nodes, nodesCount);
        }
    }

    void postorderTravel(Node *nodes[], uint &nodesCount) // idem mais avec un parcours postordonne
    {
        // fill nodes array with all nodes with postorder travel
        if (this->left != NULL)
        {
            this->left->postorderTravel(nodes, nodesCount);
        }

        if (this->right != NULL)
        {
            this->right->postorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;
    }

    Node *find(int value)
    {
        // find the node containing value
        // recherche le noeud contenant une valeur donnee
        if (this->value != value)
        {

            if (this->left != NULL)
            {
                this->left->find(value);
            }
            if (this->right != NULL)
            {
                this->right->find(value);
            }
        }
        else
        {
            return this;
        }
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }
    SearchTreeNode(int value) : Node(value) { initNode(value); }
    ~SearchTreeNode() {}
    int get_value() const { return value; }
    Node *get_left_child() const { return left; }
    Node *get_right_child() const { return right; }
};

Node *createNode(int value)
{
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
    w->show();
    return a.exec();
}
