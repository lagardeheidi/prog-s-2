#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow *w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */
void binarySearchAll(Array &array, int toSearch, int &indexMin, int &indexMax)
{
	// do not use increments, use two different binary search loop

	// initialisation des variables

	indexMin = indexMax = -1;
	int start = 0;
    int end = array.size(); // on initialise end comme la taille du tableauea
	int index = -1;

	// boucles

	while (start < end) // boucle qui recherche vers la gauchee
	{
		int middle = (start + end) / 2; // a chaque nouvelle boucle, middle est definie comme la moyenne de start et end (=milieu)
		if (toSearch > array[middle])	// Si la valeur de toSearch est >  array[middle], alorss la valeur est dans la partie driote du tableau
		{
			start = middle + 1;
		}
		else
		{
			end = middle; // sinon partie gauche ou au milieu
		}
	}

	// initialisation des variables

	indexMin = start;
	start = 0;
	end = array.size();

	while (start < end) // boucle qui recherche vers la droitee
	{
		int middle = (start + end) / 2;
		if (toSearch < array[middle])
		{
			index = middle;
			end = middle;
		}
		else
		{
			start = middle + 1;
		}
	}
	indexMax = start - 1;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
