#include <complex>

bool isInMandelbrotSet(std::complex<double> point, int maxIterations)
{
    std::complex<double> z(0, 0); // Initialisation z a 0
    int iterations = 0;           // Initialisation nombre iterations = 0
    // Boucle pour calculer z jusqua ce qu une condition d arret soit atteinte
    while (std::abs(z) <= 2 && iterations < maxIterations)
    {
        z = z * z + point; // Calcul de la prochaine d iteration de z
        iterations++;      // Incrementation du nombre iterations
    }
    // Si la condition d'arret est atteinte avant le nombre maximum iterations,
    // le point appartient a l ensemble de Mandelbrot
    return (iterations == maxIterations) ? true : false;
}