#include <iostream>
#include <string>
using namespace std;

int fibonacci(int value)
{
    if (value > 0)
    {
        if (value == 0)
        {
            return 0;
        }

        else if (value == 1)
        {
            return 1;
        }
        return fibonacci(value - 1) + fibonacci(value - 2);
    }
}

int main(int argc, char *argv[])
{
    int value = 0;
    cout << "valeur = ";
    cin >> value;
    cout << "Le terme " << value << " de la suite de Fibonacci est : " << fibonacci(value) << endl;
    return 0;
}
