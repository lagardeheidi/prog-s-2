#include <iostream>

using namespace std;
// Implémenter une structure DynaTableau et une structure Liste avec les comportements suivants :
// - ajoute(int valeur) : Ajoute une nouvelle valeur à la fin de la structure (alloue de la mémoire
// en plus si nécessaire)
// - recupere(int n) : Retourne le nième entier de la structure
// - cherche(int valeur) : Retourne l’index de valeur dans la structure ou -1 s’il n’existe pas
// - stocke(int n, int valeur) : Redéfini la nième valeur de la structure avec valeur
// La structure DynaTableau représente une structure contenant un tableau qui se réallouera quand la
// capacité de celui-ci est depassé.
// La structure Liste représente une structure qui aide à retrouver l’ensemble des données à travers
// différents noeuds chainés. Ajouter des fonctions à la structure de votre choix pour implémenter le
// comportement d’une Pile et d’une File
// - pousser_file(int valeur) : Ajoute une valeur à la fin ou au début de la structure
// - retirer_file() : Enlève la première valeur ajoutée et la retourne
// - pousser_pile(int valeur) : Ajoute une valeur à la fin ou au début de la structure
// - retirer_pile() : Enlève la dernière valeur ajoutée et la retourne

struct Noeud
{
    int donnee;
    Noeud *suivant;
};

struct Liste
{
    Noeud *premier;
    // your code
    int n;
    Noeud *dernier;
};

struct DynaTableau
{
    int *donnees;
    int taille;
    int capacite;
    // your code
};

void initialise(Liste *liste)
{
    liste->premier = nullptr;
    liste->dernier = nullptr;
    liste->n = 0;
}

bool est_vide(const Liste *liste)
{
    if (liste->n == 0) // si n = 0, alors la liste est vide
    {
        return true;
    }
    else
    {
        return false;
    }
}
// Ajoute une nouvelle valeur à la fin de la structure (alloue de la mémoire en plus si nécessaire)
void ajoute(Liste *liste, int valeur)
{
    if (est_vide(liste) == true)
    {
        liste->premier = new Noeud;        // on créé un nouveau Noeud
        liste->premier->donnee = valeur;   // On lui donne une valeur
        liste->premier->suivant = nullptr; // on donne la valeur null ptr au suivant pour sécure
    }
    Noeud *aux = liste->premier;
    while (aux->suivant != nullptr) // tant que l'aux n'est pas égal a null ptr, on parcours le tableau en allant au suivant
    {
        aux = aux->suivant;
    }
}

void affiche(const Liste *liste)
{
}

int recupere(const Liste *liste, int n)
{
    return 0;
}

int cherche(const Liste *liste, int valeur)
{
    return -1;
}

void stocke(Liste *liste, int n, int valeur)
{
}

void ajoute(DynaTableau *tableau, int valeur)
{
}

void initialise(DynaTableau *tableau, int capacite)
{
    tableau->donnees = nullptr;
}

bool est_vide(const DynaTableau *liste)
{
    if (liste->donnees == nullptr)
    {
        return true;
    }
}

void affiche(const DynaTableau *tableau)
{
}

int recupere(const DynaTableau *tableau, int n)
{
    return 0;
}

int cherche(const DynaTableau *tableau, int valeur)
{
    return -1;
}

void stocke(DynaTableau *tableau, int n, int valeur)
{
}

// void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste *liste, int valeur)
{
}

// int retire_file(Liste* liste)
int retire_file(Liste *liste)
{
    return 0;
}

// void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste *liste, int valeur)
{
}

// int retire_pile(DynaTableau* liste)
int retire_pile(Liste *liste)
{
    return 0;
}

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i = 1; i <= 7; i++)
    {
        ajoute(&liste, i * 7);
        ajoute(&tableau, i * 5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i = 1; i <= 7; i++)
    {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while (!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while (!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
