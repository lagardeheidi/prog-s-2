#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);

int search(int value, Array &toSort, int size)
{
    Context _("search", value, size); // do not care about this, it allow the display of call stack

    // your code
    if (size > 0)                      // verifie si la taille du tableau est superieure a 0
    {                                  // compare la valeur recherchee avec le dernier element du tableau
        if (value == toSort[size - 1]) // si c egal,
        {
            return_and_display(size - 1); // retourne l indice de cet element (size-1)
        }
        else // sinon continue sa recherche recursive dans le tableau
        {
            return_and_display(search(value, toSort, size - 1)); // trouve l'élément ou regarde si la taille du tableau est egal a 0
        }
    }
    else
    {
        return_and_display(-1); // si il a pas trouve
    }

    return_and_display(-1); // si le tableau est vide
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);               // create a window manager
    MainWindow::instruction_duration = 400;   // make a pause between instruction display
    MainWindow *w = new SearchWindow(search); // create a window for this exercice
    w->show();                                // show exercice

    return a.exec(); // main loop while window is opened
}
