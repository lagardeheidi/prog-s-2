#include <iostream>
#include <string>
using namespace std;

int power(int value, int n)
{
    if (n > 1)
    {
        value *= power(value, n - 1);
    }
    return value;
}

int main(int argc, char *argv[])
{
    int value = 0;
    int n;

    cout << "valeur=" << endl;
    cin >> value;
    cout << "puissance=" << endl;
    cin >> n;

    // Afficher le résultat
    cout << "Le résultat est : " << power(value, n) << endl;

    return 0;
}
