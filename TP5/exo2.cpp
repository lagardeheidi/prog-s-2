#include <QApplication>
#include <QString>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <string>

#include <tp5.h>

MainWindow *w = nullptr;
using std::size_t;
using std::string;

std::vector<string> TP5::names(
    {"Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
     "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
     "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
     "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
     "Fanny", "Jeanne", "Elo"});

unsigned long int hash(string key)
{
    // return an unique hash id from key
    int i = 0;
    int result = 0;
    while (key[i] != '\0') // tant que le caractere a indice i n est pas le caractere nul
    {
        int puissance = key.size() - (i + 1);        // calcule la puissance en utilisant la taille de la chaine - i+1
        result += (int)key[i] * pow(128, puissance); // multiplie le code ascii du caractere a l indice i mis a la puissance puis ajoute le resultat a la variable result
        i++;                                         // passe au caractere suivant
    }
    return result;
}

struct MapNode : public BinaryTree
{

    string key;
    unsigned long int key_hash;

    int value;

    MapNode *left;
    MapNode *right;

    MapNode(string key, int value) : BinaryTree(value)
    {
        this->key = key;
        this->value = value;
        this->key_hash = hash(key);

        this->left = this->right = nullptr;
    }

    /**
     * @brief insertNode insert a new node according to the key hash
     * @param node
     */
    void insertNode(MapNode *node) // compare la valeur de hashage du noeud "node" avec le noeud actuel
    {
        if (node->key_hash > this->key_hash) // si node est > au noeud actuel
        {                                    // le noeud node doit etre place a droite de l arbre
            if (this->right == NULL)         // si le noeud droit actuel est nul
            {
                this->right = node; // node peut etre inseree
            }
            else
            {
                this->right->insertNode(node); // sinon on parcourt l arbre et on lui troive une autre place
            }
        }
        else // pareil mais a gauche cette fois
        {
            if (this->left == NULL)
            {
                this->left = node;
            }
            else
            {
                this->left->insertNode(node);
            }
        }
    }

    void insertNode(string key, int value)
    {
        this->insertNode(new MapNode(key, value));
    }

    virtual ~MapNode() {}
    QString toString() const override { return QString("%1:\n%2").arg(QString::fromStdString(key)).arg(value); }
    Node *get_left_child() const { return left; }
    Node *get_right_child() const { return right; }
};

struct Map
{
    Map()
    {
        this->root = nullptr;
    }

    /**
     * @brief insert create a node and insert it to the map
     * @param key
     * @param value
     */
    void insert(string key, int value)
    {
        if (this->root == nullptr) // verifie si la racine de l arbre est nulle
        {
            this->root = new MapNode(key, value); // si oui on cree un nouveau noeud  puis on le definie comme racine de l arbre
        }
        else
        {
            this->root->insertNode(key, value); // sinon, on le place a un endroit approprie grace a insertNode
        }
    }

    /**
     * @brief get return the value of the node corresponding to key
     * @param key
     * @return
     */
    int get(string key)
    {
        int hashage = hash(key); // calcule la valeur de hashage de la cle key (pour la convertir en valeur numerique)
        MapNode *pointeur = this->root;
        while (pointeur != NULL) // tant que le pointeur n est pas nulle
        {
            if (pointeur->key_hash == hashage) // compare la valeur de hachage du noeud pointe par pointeur avec la valeur de hachage calculee precedemment
            {
                return pointeur->value; // renvoie la valeur du noeud pointe par pointeur
            }
            else
            {
                if (hashage > pointeur->key_hash) // si valeur de hashage est >valeur de hashage du noeud pointe
                {
                    pointeur = pointeur->right; // la clee se trouve a droite
                }
                else // sinon
                {
                    pointeur = pointeur->left; // elle est a gauche
                }
            }
        }
        return -1;
    }

    MapNode *root;
};

int main(int argc, char *argv[])
{
    srand(time(NULL));
    Map map;
    std::vector<std::string> inserted;

    map.insert("Yolo", 20);
    for (std::string &name : TP5::names)
    {
        if (rand() % 3 == 0)
        {
            map.insert(name, rand() % 21);
            inserted.push_back(name);
        }
    }

    printf("map[\"Margot\"]=%d\n", map.get("Margot"));
    printf("map[\"Jolan\"]=%d\n", map.get("Jolan"));
    printf("map[\"Lucas\"]=%d\n", map.get("Lucas"));
    printf("map[\"Clemence\"]=%d\n", map.get("Clemence"));
    printf("map[\"Yolo\"]=%d\n", map.get("Yolo"));
    printf("map[\"Tanguy\"]=%d\n", map.get("Tanguy"));

    printf("\n");
    for (size_t i = 0; i < inserted.size() / 2; i++)
        printf("map[\"%s\"]=%d\n", inserted[i].c_str(), map.get(inserted[i]));

    std::cout.flush();

    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new MapWindow(*map.root);
    w->show();
    return a.exec();
}
