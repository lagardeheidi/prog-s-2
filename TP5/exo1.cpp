#include <tp5.h>
#include <QApplication>
#include <time.h>

MainWindow *w = nullptr;

std::vector<string> TP5::names(
    {"Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
     "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
     "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
     "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
     "Fanny", "Jeanne", "Elo"});

int HashTable::hash(std::string element)
{
    // use this->size() to get HashTable size
    int TableAscii;
    TableAscii = (int)element[0]; // on lui attribue le premier caractère de la chaine element

    if (TableAscii > this->size()) // si table ascii est superieur a la taille de la table de hashage
    {
        TableAscii = TableAscii % this->size(); // ajuste la table pour la ramener dans la plage des indices valides
    }

    return TableAscii;
}

void HashTable::insert(std::string element)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    this->get(hash(element)) = element; // insere l element dans la table de hashage
}

/**
 * @brief buildHashTable: fill the HashTable with given names
 * @param table table to fiil
 * @param names array of names to insert
 * @param namesCount size of names array
 */
void buildHashTable(HashTable &table, std::string *names, int namesCount)
{
    table.insert(names[namesCount - 1]); // insert le dernier nom du tableau
    if (namesCount > 1)                  // verifie si il reste encore des noms a inserer dans la table
    {
        buildHashTable(table, names, namesCount - 1);
    }
}

bool HashTable::contains(std::string element)
{
    // Note: Do not use iteration (for, while, ...)
    int TableAscii = hash(element);       // permet obtenir l indice correspondant a l element dans la table de hashage
    if (this->get(TableAscii) == element) // on compare l element stocke avec l element donne en parametre
    {
        return true; // si identique, l element est present dans la table de hashage
    }
    else
    {
        return false;
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 10;
    w = new HashWindow();
    w->show();

    return a.exec();
}
